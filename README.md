# TICKETHUB
link:
https://legacy.devchallenges.io/challenges/wP0LbGgEeKhpFHUpPpDh


A brief description of what this project does and who it's for


## Acknowledgements

 - [Awesome Readme Templates](https://awesomeopensource.com/project/elangosundar/awesome-README-templates)
 - [Awesome README](https://github.com/matiassingers/awesome-readme)
 - [How to write a Good readme](https://bulldogjob.com/news/449-how-to-write-a-good-readme-for-your-github-project)


## Documentation

[Documentation](https://linktodocumentation)


## Deployment

To deploy this project run

```bash
  npm run deploy
```


## 🚀 About Me
I'm a full stack developer...


## Installation

Install my-project with npm

```bash
  npm install my-project
  cd my-project
```
    
## Tech Stack

**Client:** React, Redux, TailwindCSS

**Server:** Node, Express

