package com.chinosan.tickethub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TickethubApplication {

	public static void main(String[] args) {
		SpringApplication.run(TickethubApplication.class, args);
	}

}
