import { ChangeEvent,KeyboardEvent, useState } from "react"

export default function useInput() {
  const [text, setText] = useState("")
  function onHandleChange(e: ChangeEvent<HTMLInputElement>) {
    setText(e.target.value)
  }
  function isTextEmpty(): boolean {
    if (text.length == 0 || text == "")
      return true;
    return false;
  }
  function clearText() {
    setText("")
  }
  function onPressKeyEnter(e: KeyboardEvent<HTMLInputElement>, onHandle: () => void) {
    if (e.key == "Enter") {
      onHandle()
    }

  }
  return {
    text,
    onHandleChange,
    isTextEmpty,
    clearText,
    onPressKeyEnter
  }
}
