import { useState } from "react"

export default function useModal() {
  const [open, setOpen] = useState(false)

  function toggleModalOpen() {
    setOpen(prev => !prev)
  }
  function closeModal() {
    setOpen(false)
  }
  function openModal() {
    setOpen(true)
  }

  return {
    isModalOpen: open,
    toggleModalOpen,
    closeModal,
    openModal
  }
}
