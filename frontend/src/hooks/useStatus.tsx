import React, { useState } from 'react'

export default function useStatus() {
  const [status, setStatus] = useState<Status>("loading")
  function changeStatus(newState: Status) {
    setStatus(newState)
  }
  return {
    status,
    changeStatus
  }
}

type Status = "loading" | "error" | "success" | "default"