import { UniqueIdentifier } from "@dnd-kit/core";
import { useBoardStore } from "../context/useBoardStore";
import { DNDType } from "../types/BoardTypes";


export default function useBoardHook() {
  const board = useBoardStore()

  function addContainer(title: string) {
    const newContainer: DNDType = {
      id: crypto.randomUUID(),
      items: [],
      title
    }
    board.addContainer(newContainer)
  }
  function addTask(target: string, containerId: UniqueIdentifier) {
    if (!target) return;
    const id = `item-${crypto.randomUUID()}`;
    const container = board.containers.find((item) => item.id === containerId);
    if (!container) {
      alert("contenedor padre no encontrado")
      return;
    }
    const newItems = [...container.items, { id, title: target }]
    board.resetContainers(
      board.containers.map(container => {
        if (container.id != containerId)
          return container
        return { ...container, items: newItems }
      })
    )
  }
  function moveTask() {
    board.resetContainers([])
  }
  return {
    containers: board,
    addContainer,
    changeTaskContainer: moveTask,
    addTask
  }
}