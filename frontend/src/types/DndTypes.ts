import { UniqueIdentifier } from '@dnd-kit/core';
import { Dispatch, SetStateAction } from 'react';
import { Task } from './BoardTypes';

export type DroppableProps = {
  id: UniqueIdentifier;
  children: React.ReactNode;
  title?: string;
  description?: string;
  addNewItem: (title: string) => void;
  onAddItem: () => void
}

export type DraggableProps = {
  id: UniqueIdentifier;
  title: string;
  data?: Task
}


export type ModalProps = {
  children: React.ReactNode;
  showModal: boolean;
  setShowModal: Dispatch<SetStateAction<boolean>> | ((status?:boolean) => void);
  containerClasses?: string;
  size: "sm" | "md" | "lg" | "xl"
}