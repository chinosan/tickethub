import { UniqueIdentifier } from "@dnd-kit/core";

export type DNDType = {
  id: UniqueIdentifier;
  title: string;
  items: BoardItem[];
};

export type BoardItem = {
  id: UniqueIdentifier;
  title: string;
  data?: Task
}

export type Task = {
  members: Member[],
  description: string,
  tags: TaskTag[],
  cover: string,
  attatchment: Attatchment
}
export type TaskTag = "Tech" | "Analist" | "Coding"

export type Member = {
  uid: string,
  name: string,
  avatar: string
}
export type Attatchment = {
  filename: string,
  type: string,
  url: string
}

export type ListBoard = {
  id: UniqueIdentifier,
  title: string,
  items: TaskBoard[]
}

export type TaskBoard = {
  id: UniqueIdentifier,
  title: string,
}

export type CommentProps = {
  id:string,
  avatar: string,
  username: string,
  createdAt: Date,
  content: string
}
