export const MEMBERS_SAMPLE = [
  {
    uid: "asdn9a8sndva9da",
    avatar: "https://img.freepik.com/free-psd/3d-illustration-person-with-sunglasses_23-2149436188.jpg?size=626&ext=jpg",
    name: "Carlos Juan Iban",
  }, {
    uid: "asdn9axj1293",
    avatar: "",
    name: "Jose Moreno",
  }, {
    uid: "lkj28kj49a8sndva9da",
    avatar: "https://img.daisyui.com/images/stock/photo-1534528741775-53994a69daeb.jpg",
    name: "Hyemi Chung",
  }
]

export const COVERS = [
  {
    url: "https://images.pexels.com/photos/840996/pexels-photo-840996.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=1&amp;w=500",
    name: "Gratis Hombre En Camisa De Vestir Blanca Sentado En Una Silla Rodante Negra Mientras Mira Hacia El Equipo Negro Y Sonriendo Foto de stock",
  }, {
    url: "https://images.pexels.com/photos/796602/pexels-photo-796602.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=1&amp;w=500",
    name: "Gratis Tres Macetas De Cerámica Blanca Con Plantas De Hojas Verdes Cerca De Un Cuaderno Abierto Con Un Bolígrafo En La Parte Superior Foto de stock",
  }, {
    url: "https://images.pexels.com/photos/380768/pexels-photo-380768.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=1&amp;w=500",
    name: "Gratis Plantas De Hojas Verdes Foto de stock"
  }, {
    url: "https://images.pexels.com/photos/380769/pexels-photo-380769.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=1&amp;w=500",
    name: "Gratis Hombre Sentado Frente A La Computadora Foto de stock"
  }, {
    url: "https://images.pexels.com/photos/245240/pexels-photo-245240.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=1&amp;w=500",
    name: "Gratis Mesa Redonda De Madera Negra Con 4 Sillas Foto de stock"
  }, {
    url: "https://images.pexels.com/photos/1024248/pexels-photo-1024248.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=1&amp;w=500",
    name: "Gratis Personas En El Sofá Foto de stock"
  }, {
    url: "https://images.pexels.com/photos/37347/office-sitting-room-executive-sitting.jpg?auto=compress&amp;cs=tinysrgb&amp;dpr=1&amp;w=500",
    name: "Gratis Silla Con Ruedas De Oficina Gris Cerca De Un Escritorio De Madera Marrón Frente A Un Televisor De Pantalla Plana En La Pared Pintada De Blanco Foto de stock"
  }, {
    url: "https://images.pexels.com/photos/265072/pexels-photo-265072.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=1&amp;w=500",
    name: "Gratis Macbook Apple Plateado Sobre Mesa De Madera Marrón Foto de stock"
  }, {
    url: "https://images.pexels.com/photos/1181533/pexels-photo-1181533.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=1&amp;w=500",
    name: "Gratis Dos Mujeres Frente A La Pizarra De Borrado En Seco Foto de stock"
  }, {
    url: "https://images.pexels.com/photos/2041627/pexels-photo-2041627.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=1&amp;w=500",
    name: "Gratis Tres Mujer Sentada En Una Silla Blanca Delante De La Mesa Foto de stock"
  }, {
    url:"https://images.pexels.com/photos/1036808/pexels-photo-1036808.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=1&amp;w=500",
    name:"Gratis Smartphone Blanco Junto A Una Computadora Portátil Plateada Foto de stock"
  }, {
    url:"https://images.pexels.com/photos/416320/pexels-photo-416320.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=1&amp;w=500",
    name:"Gratis Juego De Mesa De Madera Blanca Con Sillas Foto de stock"
  }
]