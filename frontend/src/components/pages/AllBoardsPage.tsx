import NewBoardModal from "../NewBoardModal";
import Logo from "../ui/logo/Logo";
import Boards from "../ui/Boards/Boards";

export default function AllBoardsPage() {
  return (
    <div
      className="p-4"
    >
      <div
        className="flex justify-between"
      >
        <Logo />
        <h1
          className="text-2xl font-bold"
        >
          All Boards
        </h1>
        <NewBoardModal />
      </div>

      <section
        className="w-full max-w-screen-2xl m-auto"
      >
        <Boards />
      </section>

    </div>
  )
}