import Drawer from "../layout/Drawer"
import Navbar from "../layout/Navbar"
import SubHeader from "../layout/SubHeader"
import ModalNewContainer from "../ui/ContainerTask/ModalNewContainer"
import { DnDContainer } from "../ui/DnD/DnDContainer"
import ModalCard from "../ui/ModalCard/ModalCard"
//import Board from "../ui/Board/Board"
//import AllBoardsPage from "./AllBoardsPage"

export const MainPage = () => {
  return (
    <div className='w-full h-screen flex flex-col drawer drawer-end'>
      <Navbar />
      <input id="my-drawer-4" type="checkbox" className="drawer-toggle" />
      <div className='flex-1 bg-slate-600 p-8 mt-12 drawer-content'>
        <SubHeader />
        {/*<Board/>*/}
        {/*
        */}
        <main>
          <div className="flex items-center justify-between gap-y-2">
            <h1 className="text-gray-800 text-3xl font-bold">Dashboard</h1>
            <ModalNewContainer />
          </div>
          <DnDContainer />
        </main>
        <ModalCard />
        {/*} <AllBoardsPage/>*/}
      </div>
      <Drawer />
    </div>
  )
}
