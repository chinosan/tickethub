import { AddIcon } from "../../../assets/icons/Icons"
import useDndContainer from "../../../hooks/useDndContainer"
import useInput from "../../../hooks/useInput"
import useModal from "../../../hooks/useModal"
import Modal from "../Modal"

export default function ModalNewContainer() {
  const { onAddContainer } = useDndContainer();
  const { isModalOpen, closeModal, openModal } = useModal()
  const { text, onHandleChange, isTextEmpty, onPressKeyEnter } = useInput()
  function handleAdd() {
    if (isTextEmpty()) {
      alert("No hay titulo")
      return;
    }
    onAddContainer(text)
    closeModal()
  }

  return (
    <>
      <button
        onClick={openModal}
        className='btn btn-info'>
        Add Container
        <AddIcon />
      </button>
      <Modal
        showModal={isModalOpen}
        setShowModal={closeModal}
        size='sm'
      >
        <div className="flex flex-col w-full items-start gap-y-4">
          <h1 className="text-gray-800 text-3xl font-bold">
            Add Container
          </h1>
          <input
            type="text"
            placeholder="Container Title"
            name="containername"
            value={text}
            onChange={onHandleChange}
            className='input input-bordered'
            onKeyDown={(e) => onPressKeyEnter(e, handleAdd)}
          />
          <button
            className='btn btn-info'
            onClick={handleAdd}>
            Add container
          </button>
        </div>
      </Modal>
    </>
  )
}
