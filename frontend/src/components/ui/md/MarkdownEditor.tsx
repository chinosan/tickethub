import { useState } from 'react'
import ReactMarkdown from 'react-markdown'
import { DocumentTextIcon, PencilSquareIcon } from '../../../assets/icons/Icons'
const SAMPLE_MD = `
---
__Advertisement :)__

- __[pica](https://nodeca.github.io/pica/demo/)__ - high quality and fast image
  resize in browser.
- __[babelfish](https://github.com/nodeca/babelfish/)__ - developer friendly
  i18n with plurals support and easy syntax.

You will like those projects!

---

# h1 Heading 8-)
## h2 Heading
### h3 Heading
#### h4 Heading
##### h5 Heading
###### h6 Heading
`

export default function MarkdownEditor() {
  const [mode, setMode] = useState<"edit" | "preview">("preview")
  const [content, setContent] = useState(SAMPLE_MD)
  function handleMode() {
    if (mode == "edit")
      setMode("preview")
    else
      setMode("edit")
  }

  return (
    <div>
      <p className='my-3 flex items-center'>
        <strong className='flex'> <DocumentTextIcon /> Description </strong>
        <button className='btn-ghost btn-sm flex ml-3' onClick={handleMode} >
          <PencilSquareIcon />
          {mode == "edit" ? "Preview" : "Edit"}
        </button>
      </p>
      {
        
        mode == "edit"
          ? <textarea className='w-full min-h-52 mb-4' value={content} onChange={(e) => setContent(e.target.value)} />
          : <ReactMarkdown children={content} />
          
      }
      {
        /*
        <textarea className='w-full min-h-40' value={content} onChange={(e) => setContent(e.target.value)} />
        <ReactMarkdown children={content} />
        */
      }

    </div>
  )
}
