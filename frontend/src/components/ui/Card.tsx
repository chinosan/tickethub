import { AddIcon, CommentIcon, PaperClipIcon } from "../../assets/icons/Icons"
import { useModalStore } from "../../context/useModalStore"

const Card = () => {
  const {toggleIsModalCardVisible} = useModalStore(state=>state)
  return (
    <div
      className="card w-96 bg-base-100 shadow-xl p-3"
      onClick={toggleIsModalCardVisible}
    >
      <figure className="px-1 pt-1">
        <img src="https://images.unsplash.com/photo-1568992687947-868a62a9f521?w=600&amp;auto=format&amp;fit=crop&amp;q=60&amp;ixlib=rb-4.0.3&amp;ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8b2ZmaWNlfGVufDB8fDB8fHww" alt="Gente sentada en sillas" className="rounded-xl" />
        {/**
         * 
        <img alt="Gente sentada en la silla" srcset="https://images.unsplash.com/photo-1568992687947-868a62a9f521?w=100&amp;auto=format&amp;fit=crop&amp;q=60&amp;ixlib=rb-4.0.3&amp;ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8b2ZmaWNlfGVufDB8fDB8fHww 100w, https://images.unsplash.com/photo-1568992687947-868a62a9f521?w=200&amp;auto=format&amp;fit=crop&amp;q=60&amp;ixlib=rb-4.0.3&amp;ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8b2ZmaWNlfGVufDB8fDB8fHww 200w, https://images.unsplash.com/photo-1568992687947-868a62a9f521?w=300&amp;auto=format&amp;fit=crop&amp;q=60&amp;ixlib=rb-4.0.3&amp;ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8b2ZmaWNlfGVufDB8fDB8fHww 300w, https://images.unsplash.com/photo-1568992687947-868a62a9f521?w=400&amp;auto=format&amp;fit=crop&amp;q=60&amp;ixlib=rb-4.0.3&amp;ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8b2ZmaWNlfGVufDB8fDB8fHww 400w, https://images.unsplash.com/photo-1568992687947-868a62a9f521?w=500&amp;auto=format&amp;fit=crop&amp;q=60&amp;ixlib=rb-4.0.3&amp;ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8b2ZmaWNlfGVufDB8fDB8fHww 500w, https://images.unsplash.com/photo-1568992687947-868a62a9f521?w=600&amp;auto=format&amp;fit=crop&amp;q=60&amp;ixlib=rb-4.0.3&amp;ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8b2ZmaWNlfGVufDB8fDB8fHww 600w, https://images.unsplash.com/photo-1568992687947-868a62a9f521?w=700&amp;auto=format&amp;fit=crop&amp;q=60&amp;ixlib=rb-4.0.3&amp;ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8b2ZmaWNlfGVufDB8fDB8fHww 700w, https://images.unsplash.com/photo-1568992687947-868a62a9f521?w=800&amp;auto=format&amp;fit=crop&amp;q=60&amp;ixlib=rb-4.0.3&amp;ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8b2ZmaWNlfGVufDB8fDB8fHww 800w, https://images.unsplash.com/photo-1568992687947-868a62a9f521?w=900&amp;auto=format&amp;fit=crop&amp;q=60&amp;ixlib=rb-4.0.3&amp;ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8b2ZmaWNlfGVufDB8fDB8fHww 900w, https://images.unsplash.com/photo-1568992687947-868a62a9f521?w=1000&amp;auto=format&amp;fit=crop&amp;q=60&amp;ixlib=rb-4.0.3&amp;ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8b2ZmaWNlfGVufDB8fDB8fHww 1000w, https://images.unsplash.com/photo-1568992687947-868a62a9f521?w=1200&amp;auto=format&amp;fit=crop&amp;q=60&amp;ixlib=rb-4.0.3&amp;ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8b2ZmaWNlfGVufDB8fDB8fHww 1200w, https://images.unsplash.com/photo-1568992687947-868a62a9f521?w=1400&amp;auto=format&amp;fit=crop&amp;q=60&amp;ixlib=rb-4.0.3&amp;ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8b2ZmaWNlfGVufDB8fDB8fHww 1400w, https://images.unsplash.com/photo-1568992687947-868a62a9f521?w=1600&amp;auto=format&amp;fit=crop&amp;q=60&amp;ixlib=rb-4.0.3&amp;ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8b2ZmaWNlfGVufDB8fDB8fHww 1600w, https://images.unsplash.com/photo-1568992687947-868a62a9f521?w=1800&amp;auto=format&amp;fit=crop&amp;q=60&amp;ixlib=rb-4.0.3&amp;ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8b2ZmaWNlfGVufDB8fDB8fHww 1800w, https://images.unsplash.com/photo-1568992687947-868a62a9f521?w=2000&amp;auto=format&amp;fit=crop&amp;q=60&amp;ixlib=rb-4.0.3&amp;ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8b2ZmaWNlfGVufDB8fDB8fHww 2000w" src="https://images.unsplash.com/photo-1568992687947-868a62a9f521?q=80&amp;w=1000&amp;auto=format&amp;fit=crop&amp;ixlib=rb-4.0.3&amp;ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8b2ZmaWNlfGVufDB8fDB8fHww" 
        sizes="(min-width: 1335px) 410.6666666666667px, (min-width: 992px) calc(calc(100vw - 88px) / 3), (min-width: 768px) calc(calc(100vw - 64px) / 2), 100vw" itemprop="thumbnailUrl" 
       </figure> loading="lazy" data-perf="lazy-loaded-img" class="tB6UZ a5VGX" style="aspect-ratio: 4821 / 2712;" data-test="photo-grid-masonry-img"></img>
         */}
      </figure>
      <div className="card-body py-3 px-1">
        <h2 className="card-title">Move anything ready here!</h2>
        {/* <p>If a dog chews shoes whose shoes does he choose?</p> */}
        <div className="my-1">
          <div className="badge badge-primary">primary</div>
          <div className="badge badge-secondary ml-2">secondary</div>
        </div>
        <div className="flex justify-between items-center">
          <div className="flex items-center">
            <div className="avatar">
              <div className="w-12 rounded">
                <img src="https://img.daisyui.com/images/stock/photo-1534528741775-53994a69daeb.jpg" alt="Tailwind-CSS-Avatar-component" />
              </div>
            </div>
            <div className="avatar ml-1 mr-1">
              <div className="w-12 rounded">
                <img src="https://img.daisyui.com/images/stock/photo-1534528741775-53994a69daeb.jpg" alt="Tailwind-CSS-Avatar-component" />
              </div>
            </div>
            <button className="btn btn-accent">
              <AddIcon></AddIcon>
            </button>
          </div>
          <div className="flex">
            <span className="flex">
              <CommentIcon /> 1
            </span>
            <span className="flex ml-2">
              <PaperClipIcon />0
            </span>
          </div>
        </div>
        {/* <div className="card-actions">
          <button className="btn btn-primary">Buy Now</button>
        </div> */}
      </div>
    </div>
  )
}

export default Card