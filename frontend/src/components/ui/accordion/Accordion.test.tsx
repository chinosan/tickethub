import { describe, test, expect, it } from 'vitest';
import { render, screen } from '@testing-library/react'
import Accordion from './Accordion';

describe('Accordion', () => {
  // test('should render Accordion component', () => { 
  //   ///
  //  })
  test('should sum two numbers', () => {
    expect(1 + 1).toBe(2)
  })
  it("Should render a contracted Accordion compnent", () => {
    render(<Accordion />)
    expect(screen.getAllByText("Accordion")).toBeDefined()
  })
})