import { useState } from "react";
import { AddIcon, SearchIcon } from "../../../assets/icons/Icons";
import AvatarWithName from "../AvatarWithName";
import useInput from "../../../hooks/useInput";
import useBoards from "../Boards/useBoards";

export default function InviteToBoard({ square, boardParent }: { square?: boolean, boardParent: string }) {
  const { text, onHandleChange, onPressKeyEnter } = useInput()
  const [visibleRows, setVisibleRows] = useState<typeof FAKE_USERS>([])
  const { addMemberToBoard, boards } = useBoards()

  const members = boards.find(b => b.id == boardParent)?.members
  //add members to board

  function handleSearch() {
    const filtered = FAKE_USERS.filter(u => u.name.includes(text) || u.username.includes(text))
    if (filtered.length > 0) {
      setVisibleRows(filtered)
    } else {
      setVisibleRows([])
    }
  }
  return (
    <details className="dropdown">
      <summary className={`btn btn-accent ${square ? "w-16 h-16" : ""} `}>
        <AddIcon />
      </summary>
      <div className="p-3 shadow menu dropdown-content z-[1] bg-base-200 rounded w-72" >
        <div className="pb-2" >
          <p className="text-lg font-semibold">Invite to board</p>
          <p>Search users you want to invite to</p>
          <div className="flex mt-2">
            <input
              className="flex-1 input input-sm"
              placeholder="User..."
              value={text} onChange={onHandleChange}
              onKeyDown={(e) => onPressKeyEnter(e, handleSearch)}
            />
            <button className="btn btn-sm btn-info" onClick={handleSearch} > <SearchIcon /> </button>
          </div>
        </div>
        <div className="max-h-96 overflow-auto" >
          {
            visibleRows.length == 0
              ? <span className="m-4" >No se encontraron resultados</span>
              : visibleRows.filter(user => !members?.find(u => u.id == user.id.toString())).map(n => (
                <AvatarWithName
                  onClick={() => addMemberToBoard(boardParent, n)}
                  user={{ name: n.username, uid: n.id.toString(), avatar: n.avatar }}
                  key={`ava-name-${n}`}
                />
              ))
          }
        </div>
      </div>
    </details>
  )
}


const FAKE_USERS = [
  {
    "id": 1,
    "name": "Leanne Graham",
    "username": "Bret",
    "email": "Sincere@april.biz",
    "address": {
      "street": "Kulas Light",
      "suite": "Apt. 556",
      "city": "Gwenborough",
      "zipcode": "92998-3874",
      "geo": {
        "lat": "-37.3159",
        "lng": "81.1496"
      }
    },
    "phone": "1-770-736-8031 x56442",
    "website": "hildegard.org",
    "company": {
      "name": "Romaguera-Crona",
      "catchPhrase": "Multi-layered client-server neural-net",
      "bs": "harness real-time e-markets"
    },
    avatar: "https://img.freepik.com/premium-photo/man-with-shirt-that-says-no-one-it_745528-3501.jpg?w=826"
  },
  {
    "id": 2,
    "name": "Ervin Howell",
    "username": "Antonette",
    "email": "Shanna@melissa.tv",
    "address": {
      "street": "Victor Plains",
      "suite": "Suite 879",
      "city": "Wisokyburgh",
      "zipcode": "90566-7771",
      "geo": {
        "lat": "-43.9509",
        "lng": "-34.4618"
      }
    },
    "phone": "010-692-6593 x09125",
    "website": "anastasia.net",
    "company": {
      "name": "Deckow-Crist",
      "catchPhrase": "Proactive didactic contingency",
      "bs": "synergize scalable supply-chains"
    },
    avatar: "https://img.freepik.com/premium-photo/woman-s-silhouette-is-shown-circle_771335-52785.jpg"
  },
  {
    "id": 3,
    "name": "Clementine Bauch",
    "username": "Samantha",
    "email": "Nathan@yesenia.net",
    "address": {
      "street": "Douglas Extension",
      "suite": "Suite 847",
      "city": "McKenziehaven",
      "zipcode": "59590-4157",
      "geo": {
        "lat": "-68.6102",
        "lng": "-47.0653"
      }
    },
    "phone": "1-463-123-4447",
    "website": "ramiro.info",
    "company": {
      "name": "Romaguera-Jacobson",
      "catchPhrase": "Face to face bifurcated interface",
      "bs": "e-enable strategic applications"
    },
    avatar: "https://img.freepik.com/premium-photo/men-logo_777576-8398.jpg"
  },
  {
    "id": 4,
    "name": "Patricia Lebsack",
    "username": "Karianne",
    "email": "Julianne.OConner@kory.org",
    "address": {
      "street": "Hoeger Mall",
      "suite": "Apt. 692",
      "city": "South Elvis",
      "zipcode": "53919-4257",
      "geo": {
        "lat": "29.4572",
        "lng": "-164.2990"
      }
    },
    "phone": "493-170-9623 x156",
    "website": "kale.biz",
    "company": {
      "name": "Robel-Corkery",
      "catchPhrase": "Multi-tiered zero tolerance productivity",
      "bs": "transition cutting-edge web services"
    },
    avatar: "https://img.freepik.com/premium-photo/free-vector-advertise-template-design-with-world-photography-day-leaflet-brochure_999886-116.jpg"
  },
  {
    "id": 5,
    "name": "Chelsey Dietrich",
    "username": "Kamren",
    "email": "Lucio_Hettinger@annie.ca",
    "address": {
      "street": "Skiles Walks",
      "suite": "Suite 351",
      "city": "Roscoeview",
      "zipcode": "33263",
      "geo": {
        "lat": "-31.8129",
        "lng": "62.5342"
      }
    },
    "phone": "(254)954-1289",
    "website": "demarco.info",
    "company": {
      "name": "Keebler LLC",
      "catchPhrase": "User-centric fault-tolerant solution",
      "bs": "revolutionize end-to-end systems"
    },
    avatar: "https://img.freepik.com/premium-photo/vector-art-people-daily-life-high-resolution_975572-6407.jpg"
  },
  {
    "id": 6,
    "name": "Mrs. Dennis Schulist",
    "username": "Leopoldo_Corkery",
    "email": "Karley_Dach@jasper.info",
    "address": {
      "street": "Norberto Crossing",
      "suite": "Apt. 950",
      "city": "South Christy",
      "zipcode": "23505-1337",
      "geo": {
        "lat": "-71.4197",
        "lng": "71.7478"
      }
    },
    "phone": "1-477-935-8478 x6430",
    "website": "ola.org",
    "company": {
      "name": "Considine-Lockman",
      "catchPhrase": "Synchronised bottom-line interface",
      "bs": "e-enable innovative applications"
    },
    avatar: "https://img.freepik.com/premium-photo/illustration-man-with-collar-it_771335-52802.jpg?w=826"
  },
  {
    "id": 7,
    "name": "Kurtis Weissnat",
    "username": "Elwyn.Skiles",
    "email": "Telly.Hoeger@billy.biz",
    "address": {
      "street": "Rex Trail",
      "suite": "Suite 280",
      "city": "Howemouth",
      "zipcode": "58804-1099",
      "geo": {
        "lat": "24.8918",
        "lng": "21.8984"
      }
    },
    "phone": "210.067.6132",
    "website": "elvis.io",
    "company": {
      "name": "Johns Group",
      "catchPhrase": "Configurable multimedia task-force",
      "bs": "generate enterprise e-tailers"
    },
    avatar: "https://img.freepik.com/premium-photo/man-with-face-circle-with-word-man-it_798164-934.jpg"
  },
  {
    "id": 8,
    "name": "Nicholas Runolfsdottir V",
    "username": "Maxime_Nienow",
    "email": "Sherwood@rosamond.me",
    "address": {
      "street": "Ellsworth Summit",
      "suite": "Suite 729",
      "city": "Aliyaview",
      "zipcode": "45169",
      "geo": {
        "lat": "-14.3990",
        "lng": "-120.7677"
      }
    },
    "phone": "586.493.6943 x140",
    "website": "jacynthe.com",
    "company": {
      "name": "Abernathy Group",
      "catchPhrase": "Implemented secondary concept",
      "bs": "e-enable extensible e-tailers"
    },
    avatar: "https://img.freepik.com/premium-photo/man-with-blue-pink-face-red-blue-circle-with-word-choice-it_798164-932.jpg"
  },
  {
    "id": 9,
    "name": "Glenna Reichert",
    "username": "Delphine",
    "email": "Chaim_McDermott@dana.io",
    "address": {
      "street": "Dayna Park",
      "suite": "Suite 449",
      "city": "Bartholomebury",
      "zipcode": "76495-3109",
      "geo": {
        "lat": "24.6463",
        "lng": "-168.8889"
      }
    },
    "phone": "(775)976-6794 x41206",
    "website": "conrad.com",
    "company": {
      "name": "Yost and Sons",
      "catchPhrase": "Switchable contextually-based project",
      "bs": "aggregate real-time technologies"
    },
    avatar: "https://img.freepik.com/premium-photo/sportsthemed-avatar-representing-favorite-athlete-ai-generated_731790-12444.jpg"
  },
  {
    "id": 10,
    "name": "Clementina DuBuque",
    "username": "Moriah.Stanton",
    "email": "Rey.Padberg@karina.biz",
    "address": {
      "street": "Kattie Turnpike",
      "suite": "Suite 198",
      "city": "Lebsackbury",
      "zipcode": "31428-2261",
      "geo": {
        "lat": "-38.2386",
        "lng": "57.2232"
      }
    },
    "phone": "024-648-3804",
    "website": "ambrose.net",
    "company": {
      "name": "Hoeger LLC",
      "catchPhrase": "Centralized empowering task-force",
      "bs": "target end-to-end models"
    },
    avatar: "https://img.freepik.com/premium-photo/man-with-yellow-collar-yellow-scarf-is-shown_771335-52789.jpg"
  }
]