import { MouseEventHandler } from "react";
import { Member } from "../../types/BoardTypes";

export default function AvatarWithName({ user, onClick }: { user: Member, onClick: MouseEventHandler<HTMLButtonElement> }) {
  return (
    <button className="btn flex justify-start mb-3" onClick={onClick} >
      <div
        style={{
          width: 50, height: 50, borderRadius: 10, backgroundColor: "darkgray", display: "grid", placeItems: "center",
          backgroundImage: user.avatar, backgroundSize: "cover", backgroundPosition: "center", backgroundRepeat: "no-repeat"
        }}
      >MC</div>
      <p className="font-semibold ml-2" >{user.name}</p>
    </button>
  )
}
