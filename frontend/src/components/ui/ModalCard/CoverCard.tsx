import { PhotoIcon, SearchIcon } from "../../../assets/icons/Icons";
import { COVERS } from "../../../constants/constants";

export default function CoverCard({ setCover }: { setCover: (newCard: string) => void }) {
  function handlePick(url: string) {
    //alert(`Cover ${name}`)
    setCover(url)
  }
  return (
    <details className="dropdown flex-1">
      <summary className="btn w-full flex">
        <PhotoIcon />
        Cover
      </summary>
      <ul className="dropdown-content bg-slate-500 p-3 w-70 z-[1] rounded mt-2 shadow">
        <p className="font-semibold">Photo Search</p>
        <p>Search for photos</p>
        <div className="flex my-4 shadow">
          <input className="input input-sm" placeholder="keywords" />
          <button className="btn btn-sm btn-info">
            <SearchIcon />
          </button>
        </div>
        <div className="grid grid-cols-4 gap-3 mt-3">
          {
            COVERS.map(cover => (
              <img
                src={cover.url}
                className="w-full bg-blue-500 aspect-square rounded-md hover:scale-105"
                alt={cover.name}
                key={`cover-${cover.url}`}
                onClick={() => handlePick(cover.url)}
              />
            ))
          }
        </div>
      </ul>
    </details>
  )
}