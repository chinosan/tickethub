export default function LabelCard() {
  return (
    <details className="dropdown">
      <summary className="btn mt-2 w-full">Label</summary>
      <section className="dropdown-content bg-slate-700 p-3 z-[1] rounded-md mt-3">
        <p className="font-semibold">Label</p>
        <p>Select a name and color</p>

        <input type="text" className="input input-sm my-4" placeholder="Label..." />
        <div className="grid grid-cols-4 gap-3">
          {
            [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map(n => (
              <div className="bg-blue-500" key={`label-${n}`}>{n}</div>
            ))
          }
        </div>
        <p className="font-semibold mt-3" >Available</p>
        <div>
          <p className="badge badge-primary">primary</p>
          <p className="badge badge-secondary ml-2">secondary</p>
        </div>
        <button className="btn btn-info mt-3">Add</button>
      </section>
    </details>
  )
}