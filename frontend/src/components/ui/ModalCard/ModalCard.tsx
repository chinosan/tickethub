import { useEffect } from "react";
import { AddIcon, DocumentTextIcon, SearchIcon, UserIcon, UsersIcons } from "../../../assets/icons/Icons";
import { useBoardStore } from "../../../context/useBoardStore";
import { useModalStore } from "../../../context/useModalStore";
//import useModal from "../../../hooks/useModal";
import Attachment from "../Attatchment/Attachment";
import AvatarWithName from "../AvatarWithName";
import Modal from "../Modal";
import Comment from "../comment/Comment";
import NewComment from "../comments/NewComment";
import CoverCard from "./CoverCard";
import LabelCard from "./LabelCard";
import MarkdownEditor from "../md/MarkdownEditor";
import Comments from "../comments/Comments";
import { fakeUsers } from "../Boards/boardsStore";

const COVER_NEW_TASK = "https://plus.unsplash.com/premium_photo-1706430115968-e4524fc70835?q=80&amp;w=1000&amp;auto=format&amp;fit=crop&amp;ixlib=rb-4.0.3&amp;ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwxN3x8fGVufDB8fHx8fA%3D%3D";
const SAMPLE_MD = `
---
__Advertisement :)__

- __[pica](https://nodeca.github.io/pica/demo/)__ - high quality and fast image
  resize in browser.
- __[babelfish](https://github.com/nodeca/babelfish/)__ - developer friendly
  i18n with plurals support and easy syntax.

You will like those projects!

---

# h1 Heading 8-)
## h2 Heading
### h3 Heading
#### h4 Heading
##### h5 Heading
###### h6 Heading


## Horizontal Rules

___

---

***


`

export default function ModalCard() {
  let cover = COVER_NEW_TASK;
  //const { isModalOpen, openModal } = useModal()
  const { currentTask, clearCurrentTask } = useBoardStore(state => state);
  const { isModalCardVisible, toggleIsModalCardVisible } = useModalStore(state => state);
  useEffect(() => {
    return () => {
      clearCurrentTask()
    }
  }, [])
  return (
    <Modal showModal={isModalCardVisible} setShowModal={() => toggleIsModalCardVisible()} size='xl' >
      <div className='bg-zinc-700' >
        <img src={cover} className='h-56 max-w-full mx-auto rounded-lg' loading='lazy' />
        <div className='grid grid-cols-3 gap-4 mt-4' >
          <div className='col-span-2'>
            <h2 className='text-xl font-semibold'>{currentTask?.title}</h2>
            <span className='text-sm'>In list: <strong>In Progress</strong></span>
            
            <MarkdownEditor  />
            
            <Attachment />
            <Comments/>
          </div>
          <div>
            <h3 className='text-xl font-semibold flex'> <UserIcon /> Actions </h3>
            <div className='flex flex-col'>
              <button className='btn mt-2'>Members</button>
              <LabelCard />
              <div className="h-2"></div>
              <CoverCard setCover={(newCover:string)=>cover=newCover} />

              {/*  This shown when thay are members, if not only button members */}
              <h3 className='text-xl font-semibold mt-4 flex'> <UsersIcons /> Members</h3>
              <details className="dropdown">
                <summary className="btn btn-accent">
                  Assign a member
                  <AddIcon />
                </summary>
                <div className="p-3 shadow menu dropdown-content z-[1] bg-base-200 rounded w-72" >
                  <div className="pb-2" >
                    <p className="text-lg font-semibold">Members</p>
                    <p>Assign members to this card</p>
                    <div className="flex mt-2">
                      <input className="flex-1 input input-sm" placeholder="User..." />
                      <button className="btn btn-sm btn-info" > <SearchIcon /> </button>
                    </div>
                  </div>
                  {
                    fakeUsers.map(n => <AvatarWithName key={`ava-name-${n.id}`} user={{avatar:n.avatar,name:n.name,uid:n.id}} onClick={()=>alert(`user ${n.name}}`)} />)
                  }
                  <button className='btn btn-sm mt-3'>Inivite</button>
                </div>
              </details>
            </div>
            <div>

            </div>
          </div>
        </div>
      </div>
    </Modal>
  )
}
