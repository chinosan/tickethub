import { useState } from 'react'
import { LockIcon, WorldIcon } from '../../../assets/icons/Icons'

type Props = {
  value: "public" | "private",
  onChange: (val: "public" | "private") => void,
  className?: string
}

export default function DropdownChooseVisibility({ value = "private", onChange, className }: Props) {
  const [visivility, setVisivility] = useState(value)

  function handleChange(newVal: "public" | "private") {
    setVisivility(newVal)
    onChange(newVal)
  }

  return (
    <details className="dropdown flex-1 flex">
      <summary className={`btn ${className}`}>
        {
          visivility == "public"
            ? <>
              <LockIcon />
              Private
            </>
            : <>
              <WorldIcon />
              Public
            </>
        }
      </summary>
      <ul className="p-2 shadow menu dropdown-content z-[1] bg-base-100 rounded-box w-64">
        <div className="p-1">
          <p className="text-lg font-semibold">Visibility</p>
          <span className="text-xs">Choose who can see this board</span>
        </div>
        <li><button className="btn mt-1 btn-lg" onClick={() => handleChange("public")} >
          <div className="flex flex-col">
            <p className="flex items-center"> <WorldIcon /> Public </p>
            <p className="text-xs font-light mt-1">Anyone in the internet can see this</p>
          </div>
        </button></li>
        <li><button className="btn mt-1 btn-lg" onClick={() => handleChange("private")}>
          <div className="flex flex-col">
            <p className="flex items-center"> <LockIcon /> Private </p>
            <p className="text-xs font-light mt-1">Only board menbers can see this</p>
          </div>
        </button></li>
      </ul>
    </details>
  )
}
