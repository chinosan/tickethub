import React, { useState } from 'react'
import useStatus from '../../../hooks/useStatus'
import { Board, boardsStore, fakeBoards, fakeUsers } from './boardsStore';
import { Member } from '../../../types/BoardTypes';

export default function useBoards() {
  const { boards, clearBoards, setBoards, updateBoard } = boardsStore(state => state);
  const { changeStatus, status } = useStatus();

  async function getBoards() {
    try {
      if (boards.length > 0){
        changeStatus("success")
        return;
      }
      const res = await new Promise((resolve, reject) => {
        try {
          setTimeout(() => {
            resolve(fakeBoards)
          }, 3000)
        } catch (error) {
          reject(error)
        }
      })
      setBoards(res as Board[])
      changeStatus("success")
    } catch (error) {
      changeStatus("error")
    }
  }
  function addBoard(item: Board) {
    setBoards([...boards, item])
  }

  function addMemberToBoard(boardId: string, newMember: User) {
    let finded = boards.find(b => b.id == boardId)
    if (finded) {
      updateBoard({ ...finded, members: [...finded.members, newMember] })
    }
  }

  return {
    status,
    boards,
    getBoards,
    clearBoards,
    addBoard,
    addMemberToBoard
  }
}
