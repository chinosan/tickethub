import { create } from "zustand";
import { COVERS, MEMBERS_SAMPLE } from "../../../constants/constants";
import { DNDType } from "../../../types/BoardTypes";

type Props = {
  boards: Board[],
  setBoards: (boards: Board[]) => void,
  clearBoards: () => void,
  addMember: (user: User[]) => void,
  updateBoard: (board: Board) => void
}

export const boardsStore = create<Props>()((set) => ({
  boards: [],
  setBoards: (newBoards) => set(() => ({ boards: newBoards })),
  clearBoards: () => set(() => ({ boards: [] })),
  addMember: (user) => set((state) => {
    const img = state.boards.find(b => b.id)
    return ({ boards: state.boards })
  }),
  updateBoard: (board: Board) => set((state) => ({ boards: state.boards.map(b => b.id == board.id ? board : b) }))
}))

export type Board = {
  id: string,
  title: string,
  visivility: "public" | "private",
  cover: string,
  created: {
    createdBy: {
      author: {
        name: string,
        id: string,
        avatar: string
      }
    },
    createdAt: Date,
  },
  members: User[],
  description: string,
  containers: DNDType[]
}
type User = {
  name: string,
  id: string,
  avatar: string
}


export const fakeUsers = [
  {
    name: MEMBERS_SAMPLE[0].name,
    id: MEMBERS_SAMPLE[0].uid,
    avatar: MEMBERS_SAMPLE[0].avatar
  }, {
    name: MEMBERS_SAMPLE[1].name,
    id: MEMBERS_SAMPLE[1].uid,
    avatar: MEMBERS_SAMPLE[1].avatar
  }, {
    name: MEMBERS_SAMPLE[2].name,
    id: MEMBERS_SAMPLE[2].uid,
    avatar: MEMBERS_SAMPLE[2].avatar
  }
]

export const fakeBoards = [{
  id: "an9s8vda89vd9823e",
  title: "Board 1",
  visivility: "public",
  cover: COVERS[2].url,
  created: {
    createdBy: {
      author: {
        name: "Start Treek",
        id: "2",
        avatar: "https://ps.w.org/user-avatar-reloaded/assets/icon-256x256.png?rev=2540745"
      }
    },
    createdAt: new Date(),
  },
  members: [fakeUsers[2], fakeUsers[1]],
  description: "Some static file hasve changedLorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur, dolorum."
}, {
  id: "90m2z81w6t7csasd",
  title: "Board 2",
  visivility: "public",
  cover: COVERS[1].url,
  created: {
    createdBy: {
      author: {
        name: "Start Treek",
        id: "2",
        avatar: "https://ps.w.org/user-avatar-reloaded/assets/icon-256x256.png?rev=2540745"
      }
    },
    createdAt: new Date(),
  },
  members: [fakeUsers[0], fakeUsers[1]],
  description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur, dolorum."
}]
