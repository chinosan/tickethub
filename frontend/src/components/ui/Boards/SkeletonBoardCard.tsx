
export default function SkeletonBoardCard() {
  return (
    <div className="shadow rounded p-6 bg-slate-600">
      <div className="skeleton w-full aspect-video"></div>
      <div className="skeleton h-4 w-20 my-6"></div>
      <div className="flex gap-2" >
        <div className="skeleton h-16 w-16" ></div>
        <div className="skeleton h-16 w-16" ></div>
        <div className="skeleton h-16 w-16" ></div>
      </div>
    </div>
  )
}
