import { useEffect } from "react";
import { Link } from "react-router-dom";
import useBoards from "./useBoards";
import SkeletonBoardCard from "./SkeletonBoardCard";
import InviteToBoard from "../invite/InviteToBoard";

const DEFAULT_COVER = "https://images.unsplash.com/photo-1515378791036-0648a3ef77b2?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTV8fG9mZmljZXxlbnwwfHwwfHx8MA%3D%3D"
export default function Boards() {
  const { boards, getBoards, status } = useBoards()

  useEffect(() => {
    getBoards()
  }, [])

  if (status == "loading")
    return (
      <div>
        <span className="loading loading-spinner loading-lg"></span>
        <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-8  rounded shadow m-2">
          {
            [1, 2, 3].map(n => <SkeletonBoardCard key={`skeleton-${n}`} />)
          }
        </div>
      </div>
    )

  if (status == "error")
    return <p>Error</p>

  return (
    <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-8  rounded shadow m-2">
      {
        boards.map(board => (
          <div key={`bord-${board.title}`} className="shadow rounded p-6 bg-slate-900 " >
            <img
              src={board.cover ? board.cover : DEFAULT_COVER}
              className="w-full aspect-video rounded-lg" />
            <Link to={"/main"} >
              <h2 className="text-xl font-bold mt-4">{board.title}</h2>
            </Link>
            <div className="flex items-center my-4">
              {
                (board.members.length > 2 ? board.members.slice(0, 2) : board.members).map(n => (
                  <div
                    key={`memberboars-${n.id}-${board.title}`}
                    className="tooltip" data-tip={n.name}
                  >
                    <div
                      className="w-16 h-16 rounded bg-slate-400 grid place-items-center mr-2"
                    >
                      {n.name.split(" ").map(w => w[0])}
                    </div>
                  </div>
                ))
              }
              {
                board.members.length > 2 && (
                  <div className="group" >
                    <span>+5 others</span>
                    <div className="absolute ml-10 hidden group-hover:block z-10 transition ease-in-out delay-150 " >
                      <ul className="bg-black opacity-90 p-3" >
                        <li>asd12345asd</li>
                        {
                          board.members.slice(2).map(m => (<li key={`board-member-extra-${m.id}`} >{m.name}</li>))
                        }
                      </ul>
                    </div>
                  </div>
                )
              }

              <InviteToBoard boardParent={board.id} square />
            </div>
          </div>

        ))
      }
    </div>
  )
}
