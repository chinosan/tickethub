import { CommentProps } from "../../../types/BoardTypes"


export default function Comment({ comment, onDelete, onEdit }: { comment: CommentProps, onDelete: () => void, onEdit: () => void }) {
  return (
    <div className="my-3" >
      <div className="flex">
        <div className="avatar">
          <div className="w-16 rounded">
            <img src={comment.avatar} />
          </div>
        </div>
        <div className="flex-1 ml-4">
          <h6 className="font-bold"> 
          {comment.username} 
          </h6>
          <p className="text-sm"> 
          {comment.createdAt.toISOString().split("T")[0]} 
          </p>
        </div>
        <div>
          <button className="btn btn-sm btn-ghost" onClick={onEdit}>edit</button>
          -
          <button className="btn btn-sm btn-ghost" onClick={onDelete} >delete</button>
        </div>
      </div>
      <div className="py-3"> 
      {comment.content}
      </div>
    </div>
  )
}
