import { useDragAndDrop } from "@formkit/drag-and-drop/react"


export default function Board() {
  const items = ["one", "two", "three"]
  const doneItems = ["four", "five"]
  const [todoList, todos] = useDragAndDrop<HTMLUListElement, string>(
    items,
    {
      group: "board"
    }
  )
  const [completedList, completed] = useDragAndDrop<HTMLUListElement, string>(
    doneItems,
    { group: "board" }
  )
  const reviwedItems = ["Ten", "Eleven"];
  const [reviewList, reviwed] = useDragAndDrop<HTMLUListElement, string>(reviwedItems, { group: "board" })

  return (
    <div className="flex gap-3 bg-base-300 p-4 rounded">
      <ul className="p-4 bg-red-700 w-52" ref={todoList} >
        {todos.map(todo => (
          <li className="p-2 bg-red-600 my-1" key={todo}>
            {todo}
          </li>
        ))}
      </ul>
      <ul className="p-4 bg-red-700 w-52" ref={completedList} >
        {completed.map(item => (
          <li className="p-2 bg-red-600 my-1" key={item} >
            {item}
          </li>
        ))}
      </ul>
      <ul className="p-4 bg-red-700 w-52" ref={reviewList}>
        {reviwed.map(item => (
          <li className="p-2 bg-red-600 my-1" key={item} >
            {item}
          </li>
        ))}
      </ul>
    </div>
  )
}
