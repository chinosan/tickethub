import { useState } from "react";
import { AddIcon } from "../../assets/icons/Icons";
import { AnimatePresence, motion } from "framer-motion";
import useDndContainer from "../../hooks/useDndContainer";
import useInput from "../../hooks/useInput";
import { UniqueIdentifier } from "@dnd-kit/core";

type NewCardProps = {
  //addNewItem: (title: string) => void,
  containerId: UniqueIdentifier
}

export default function NewCard(
  { containerId }: NewCardProps
) {
  const {
    addNewItem
  } = useDndContainer()
  const { text, onHandleChange, isTextEmpty, clearText, onPressKeyEnter } = useInput()
  const [showCardInput, setShowCardInput] = useState(false)
  function handleAddNewCard() {
    if (isTextEmpty()) {
      alert("El contenido esta vacio")
      return;
    }
    addNewItem(text, containerId)
    clearText()
    setShowCardInput(false)
  }
  return (
    <div>
      <AnimatePresence>
        {
          showCardInput && (
            <motion.div
              initial={{ opacity: 0, height: 0 }}
              animate={{ opacity: 1, height: "auto" }}
              exit={{ opacity: 0, height: 0 }}
            >
              <article className={`bg-primary-content rounded flex-col p-4 ${showCardInput ? "flex" : "hidden"}`} >
                <input
                  value={text}
                  onChange={onHandleChange}
                  className="input input-sm"
                  type="text"
                  placeholder="Enter a title for this card"
                  onKeyDown={(e) => onPressKeyEnter(e, () => handleAddNewCard())}
                />
                <div className="mt-4">
                  <button
                    className="btn btn-sm rounded-md"
                    onClick={handleAddNewCard}
                  >Save</button>
                  <button
                    className="btn btn-sm  btn-warning rounded-md ml-4"
                    onClick={() => setShowCardInput(false)}
                  > Cancel</button>
                </div>
              </article>
            </motion.div>
          )
        }
      </AnimatePresence>

      <button
        className="btn mt-4 hover:bg-blue-700 w-full"
        onClick={() => setShowCardInput((prev) => !prev)}
      >
        Add another card
        <AddIcon />
      </button>
    </div >
  )
}