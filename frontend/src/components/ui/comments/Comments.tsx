import NewComment from "./NewComment";
import Comment from '../comment/Comment'
import { useState } from "react";
import { CommentProps } from "../../../types/BoardTypes";

export default function Comments() {
  const [comments, setComments] = useState<CommentProps[]>(EXAMPLE_COMMENTS)
  function addComment() { }
  function deleteComment() { }
  function editComment() { }
  return (
    <div>
      <NewComment />
      {comments.map(c => (
        <Comment key={c.id} comment={c} onDelete={deleteComment} onEdit={editComment} />
      ))}
    </div>
  )
}


const EXAMPLE_COMMENTS: CommentProps[] = [
  {
    id: "9as8dn",
    avatar: "https://i.pinimg.com/474x/0a/a8/58/0aa8581c2cb0aa948d63ce3ddad90c81.jpg",
    username: "Fresh Air",
    createdAt: new Date(),
    content: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellat provident temporibus ad"
  }, {
    id: "54hb234",
    avatar: "https://ps.w.org/user-avatar-reloaded/assets/icon-256x256.png?rev=2540745",
    username: "Start Treek",
    createdAt: new Date(),
    content: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellat provident temporibus ad"
  },
] 