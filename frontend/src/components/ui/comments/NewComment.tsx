import { useRef } from "react"

export default function NewComment() {
  const newCommentRef = useRef<HTMLTextAreaElement | null>(null)

  function addNewComment() {
    if (newCommentRef.current?.value == "") {
      alert("No hay contenifo")
      return
    }
    //console.log()
  }
  return (
    <div className="p-3 rounded flex my-3 shadow bg-slate-600">
      <div>Avatar</div>
      <div className="ml-3 flex-1">
        <div className="flex flex-col">
          <textarea
            ref={newCommentRef}
            placeholder="Write a comment"
            className="h-24 textarea flex-1 bg-slate-600"
          />
          <div className="flex justify-end">
            <button
              onClick={addNewComment}
              className="btn btn-sm btn-info mt-3">
              Comment
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}