import { DocumentTextIcon } from "../../../assets/icons/Icons";

export default function Attachment() {
  return (
    <>
      <p className='my-3 flex'>
        <strong className='flex'><DocumentTextIcon />  Attatchment </strong>
        <input id="uploadFile" type="file" className='hidden' />
        <label htmlFor="uploadFile" className="btn btn-sm ml-2" >+Add</label>
      </p>
      <div className="flex my-2">
        <img
          src=""
          alt="file"
          className="w-40 rounded aspect-video bg-blue-500"
        />
        <div className="ml-4">
          <p className="opacity-70">Added July 5, 2020</p>
          <p className="font-semibold">Reasoning by Ranganath</p>
          <button className="mt-3 btn btn-outline inline-block">Download</button>
          <button className="mt-3 btn btn-outline ml-3 inline-block">Delete</button>
        </div>
      </div>
    </>
  )
}