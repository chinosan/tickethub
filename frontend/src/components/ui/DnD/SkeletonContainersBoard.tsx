
export default function SkeletonContainersBoard() {
  return (
    <div className="grid grid-cols-3 gap-4">
      {
        [1, 2, 3].map(n => (
          <div key={`skeletin-board-${n}`}  >
            <div className='flex justify-between mt-6' >
              <span className='skeleton w-20 h-8' ></span>
              <span className='skeleton w-16 h-8'></span>
              <span className=' skeleton w-12 h-8'></span>
            </div>
            <div>
              <div className=' skeleton w-full h-60 mt-4'></div>
              <span className='skelton mt-3 w-full h-12'></span>
            </div>
            <div className='skeleton w-full h-12 mt-10'></div>
          </div>
        ))
      }
    </div>
  )
}
