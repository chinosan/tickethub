import { useSortable } from '@dnd-kit/sortable';
import { CSS } from '@dnd-kit/utilities';
import { DraggableProps } from '../../../types/DndTypes';
import { AddIcon, CommentIcon, PaperClipIcon } from '../../../assets/icons/Icons';
import { useModalStore } from '../../../context/useModalStore';
import { useBoardStore } from '../../../context/useBoardStore';

export default function Draggable({ id, title, data }: DraggableProps) {
  const { openModal } = useModalStore(state => state)
  const { setCurrentTask } = useBoardStore(state => state)
  const {
    attributes,
    listeners,
    setNodeRef,
    transform,
    transition,
    isDragging,

  } = useSortable({
    id: id,
    data: {
      type: 'item',
    },
  });
  function handleOpen() {
    //alert("Open")
    openModal()
    setCurrentTask({ id, title, data })
  }
  const hideContent = isDragging ? "opacity-0" : "";
  return (
    <article
      ref={setNodeRef}
      {...attributes}
      style={{
        transition,
        transform: CSS.Translate.toString(transform),
      }}
      className={`
      card bg-base-100 shadow-xl p-3
      ${isDragging && "border-4 border-sky-500 border-dashed bg-blue-300"}
      `}
      onClick={handleOpen}
    >
      <figure className={`px-1 pt-1 ${hideContent}`} onClick={handleOpen} >
        <img src="https://images.unsplash.com/photo-1568992687947-868a62a9f521?w=600&amp;auto=format&amp;fit=crop&amp;q=60&amp;ixlib=rb-4.0.3&amp;ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8b2ZmaWNlfGVufDB8fDB8fHww" alt="Gente sentada en sillas" className="rounded-xl" />
      </figure>
      <div className={`card-body py-3 px-1 ${hideContent}`}>
        <h2 className="card-title" onClick={handleOpen} >{title}</h2>
        <button {...listeners} >Mover</button>
        {/* <p>If a dog chews shoes whose shoes does he choose?</p> */}
        <div className="my-1">
          {
            data?.tags?.map((tag, index) => (
              <div className="badge badge-primary" key={index}>
                {tag}
              </div>
            ))
          }
          <div className="badge badge-primary">primary</div>
          <div className="badge badge-secondary ml-2">secondary</div>
        </div>
        <div className="flex justify-between items-center">
          <div className="flex items-center">
            {
              data?.members?.map((user, index) => (
                <div className="avatar" key={index}>
                  <div className="w-12 rounded">
                    <img src={user.avatar} alt={user.name} />
                  </div>
                </div>
              ))
            }
            <div className="avatar">
              <div className="w-12 rounded">
                <img src="https://img.daisyui.com/images/stock/photo-1534528741775-53994a69daeb.jpg" alt="Tailwind-CSS-Avatar-component" />
              </div>
            </div>
            <div className="avatar ml-1 mr-1">
              <div className="w-12 rounded">
                <img src="https://img.daisyui.com/images/stock/photo-1534528741775-53994a69daeb.jpg" alt="Tailwind-CSS-Avatar-component" />
              </div>
            </div>
            <button className="btn btn-accent">
              <AddIcon />
            </button>
          </div>
          <div className="flex" onClick={handleOpen} >
            <span className="flex">
              <CommentIcon /> 1
            </span>
            <span className="flex ml-2">
              <PaperClipIcon />0
            </span>
          </div>
        </div>
      </div>

    </article>
  )
}
