import { Droppable } from './Droppable';
import Draggable from './Draggable';
import useDndContainer from '../../../hooks/useDndContainer';
// DnD
import { DndContext, DragOverlay, closestCorners } from '@dnd-kit/core';
import {
  SortableContext,
  //arrayMove,
  //sortableKeyboardCoordinates,
} from '@dnd-kit/sortable';
import { useEffect, useState } from 'react';
import SkeletonContainersBoard from './SkeletonContainersBoard';



export const DnDContainer = () => {

  const {
    containers,
    onAddItem,
    sensors,
    handleDragStart,
    handleDragMove,
    handleDragEnd,
    findItemTitle,
    findContainerItems,
    findContainerTitle,
    activeId,
  } = useDndContainer();

  const [isLoading, setIsLoading] = useState(true)
  useEffect(() => {
    new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(true)
      }, 2000)
    })
      .then(res => setIsLoading(false))
  }, [])

  if (isLoading) return <SkeletonContainersBoard />

  return (
    <div className="grid grid-cols-3 gap-4">
      <DndContext
        sensors={sensors}
        collisionDetection={closestCorners}
        onDragStart={handleDragStart}
        onDragMove={handleDragMove}
        onDragEnd={handleDragEnd}
      >

        <SortableContext items={containers.map((i) => i.id)}>
          {containers.map((container) => (
            <Droppable
              id={container.id}
              title={container.title}
              key={container.id}
              onAddItem={() => {
                // setCurrentContainerId(container.id);
                //setShowAddItemModal(true);
              }}
              addNewItem={(title: string) => {
                console.log("title: ", title)
                console.log("container: ", container.id)
                // setCurrentContainerId(container.id);
                // addNewItem(title)
              }}
            >
              <SortableContext items={container.items.map((i) => i.id)}>
                <div className="flex items-start flex-col gap-y-3">
                  {container.items.map((i) => (
                    <Draggable title={i.title} id={i.id} key={i.id} data={i.data} />
                  ))}
                </div>
              </SortableContext>
            </Droppable>
          ))}
        </SortableContext>

        <DragOverlay adjustScale={false}>
          {/* Drag Overlay For item Item */}
          {activeId && activeId.toString().includes('item') && (
            <Draggable id={activeId} title={findItemTitle(activeId)} />
          )}
          {/* Drag Overlay For Container */}
          {activeId && activeId.toString().includes('container') && (
            <Droppable
              addNewItem={(title: string) => {
                //setCurrentContainerId()
                //addNewItem(title)
                console.log("titulo:", title)
              }}
              onAddItem={onAddItem}
              id={activeId}
              title={findContainerTitle(activeId)}>
              {findContainerItems(activeId).map((i) => (
                <Draggable key={i.id} title={i.title} id={i.id} />
              ))}
            </Droppable>
          )}
        </DragOverlay>
      </DndContext>
    </div>
  )
}
