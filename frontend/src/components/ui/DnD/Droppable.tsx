import { useSortable } from '@dnd-kit/sortable';
import { CSS } from '@dnd-kit/utilities';
import { DroppableProps } from '../../../types/DndTypes';
import NewCard from '../NewCard';
//import Card from '../Card';

export const Droppable = ({
  id,
  children,
  title,
  description,
 // onAddItem,
 // addNewItem
}: DroppableProps) => {
  const {
    attributes,
    setNodeRef,
    listeners,
    transform,
    transition,
    isDragging,
  } = useSortable({
    id: id,
    data: {
      type: 'container',
    },
  });
  return (
    <div
      {...attributes}
      ref={setNodeRef}
      style={{
        transition,
        transform: CSS.Translate.toString(transform),
      }}
      className={`w-full h-full p-4 rounded-xl flex flex-col gap-y-4 ${isDragging && 'opacity-50'}`}
    >
      <div className="flex items-center justify-between">
        <div className="flex flex-col gap-y-1">
          <h1 className="text-gray-200 text-xl font-bold">
            {title}
          </h1>
          <p className="text-gray-400 text-sm">
            {description}
          </p>
        </div>
        <button
          className="border p-2 text-xs rounded-xl shadow-lg hover:shadow-xl"
          {...listeners}
        >
          Drag Handle
        </button>
        <details className='dropdown'>
          <summary className='btn btn-sm'>
            ...
          </summary>
          <div className='p-2 menu dropdown-content w-52 bg-base-300 z-[1] rounded shadow' >
            <button>Rename</button>
            <div className='divider my-1' />
            <button>Delete this</button>
          </div>
        </details>
      </div>

      {children}

{/**
      <Card />
 * 
 */}
      <NewCard
        containerId={id}
      //addNewItem={addNewItem}
      />

   {/*   <button className='btn-ghost text-black' onClick={onAddItem}>
        Add Item
      </button>*/}
    </div>
  )
}
