
export default function CreatedByUser() {
  return (
    <div className="flex">
      <div className="avatar">
        <div className="w-12 rounded-md">
          <img src="https://img.daisyui.com/images/stock/photo-1534528741775-53994a69daeb.jpg" alt="Tailwind-CSS-Avatar-component" />
        </div>
      </div>
      <div className="ml-4">
        <h6 className="font-bold">Jhon Dow</h6>
        <p className="text-sm">24 Agust at 20:43</p>
      </div>
    </div>
  )
}
