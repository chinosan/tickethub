import { Link } from "react-router-dom";

export default function Logo() {
  return (
    <Link to={"/"} >
      <button className="btn btn-ghost text-xl">TICKETHUB</button>
    </Link>
  )
}