
export const Grid = () => {
  const sections = ["Backlog 🤔", "In Progress 🕹️", "In Review ⚙️", "Completed 🙌"]
  return (
    <main className="grid grid-cols-5 gap-4" >
      {
        sections.map(col => (
          <section>
            <div className="flex justify-between">
              <h4 className="text-xl"> {col} </h4>
              <button className="btn btn-ghost">...</button>
            </div>
          </section>
        ))
      }
      <button className="btn btn-primary">Add another list +</button>
    </main>
  )
}
