import { Link } from "react-router-dom";
import { MenuDotsIcon } from "../../assets/icons/Icons";
import Logo from "../ui/logo/Logo";

export default function Navbar() {
  return (
    <div className="navbar bg-base-100 fixed">
      <div className="flex-1">
        <Logo />
        <p className="text-xl ml-4">Curent board</p>
        {/* <div className="h-6 w-[1px] bg-slate-200 mx-4"/> */}
        <div className="divider divider-horizontal"></div>
        <Link to={"/boards"}>
          <button className="btn btn-ghost bg-base-200">
            <MenuDotsIcon />
            All boards
          </button>
        </Link>
      </div>
      <div className="flex-none gap-2">
        <div className="form-control">
          <input type="text" placeholder="Search" className="input input-bordered w-24 md:w-auto" />
        </div>
        <div className="dropdown dropdown-end">
          <div tabIndex={0} role="button" className="btn btn-ghost btn-circle avatar">
            <div className="w-10 rounded-full">
              <img alt="Tailwind CSS Navbar component" src="https://img.daisyui.com/images/stock/photo-1534528741775-53994a69daeb.jpg" />
            </div>
          </div>
          <ul tabIndex={0} className="mt-3 z-[1] p-2 shadow menu menu-sm dropdown-content bg-base-100 rounded-box w-52">
            <li>
              <a className="justify-between">
                Profile
                <span className="badge">New</span>
              </a>
            </li>
            <li><a>Settings</a></li>
            <li><a>Logout</a></li>
          </ul>
        </div>
      </div>
    </div>
  )
}