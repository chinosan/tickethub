import { DocumentTextIcon, PencilSquareIcon, UserIcon } from "../../assets/icons/Icons";
import CreatedByUser from "../ui/CreatedByUser";
import MarkdownEditor from "../ui/md/MarkdownEditor";

export default function Drawer() {
  return (
    <div className="drawer-side">
      <label htmlFor="my-drawer-4" aria-label="close sidebar" className="drawer-overlay"></label>

      <ul className="menu p-4 w-96 min-h-full bg-base-200 text-base-content">
        <div>
          <h2 className="text-2xl mb-2">Dev board</h2>

          <h3 className="font-semibold flex my-3" >
            <UserIcon />
            Made by
          </h3>
          {/* //This component is reused on modal task */}
          <CreatedByUser />

          {/* //This component is reused on modal task 
          <h3 className="font-semibold my-3 flex items-center" >
            <DocumentTextIcon />
            Description
            <button className="btn btn-sm flex ml-2" >
              <PencilSquareIcon />
              edit
            </button>
          </h3>
          <div>
            <p>
              Lorem ipsum dolor sit amet.
              <br />
              Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dignissimos sapiente eum quam.
              <br />
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere iure aut pariatur, cumque tempora porro.
              <br />
              Lorem, ipsum dolor.
            </p>
          </div>
*/}
          <MarkdownEditor />

          <h3 className="font-semibold flex my-5">
            <DocumentTextIcon />
            Team
          </h3>
          <div className="flex flex-col">
            <div className="flex justify-between mt-2 items-center">
              <div style={{ width: 50, height: 50, backgroundColor: "whitesmoke", borderRadius: 10 }}>Av</div>
              <p>User Name</p>
              <button className="btn btn-ghost">Admin</button>
            </div>
            <div className="flex justify-between mt-2 items-center">
              <div style={{ width: 50, height: 50, backgroundColor: "whitesmoke", borderRadius: 10 }}>Av</div>
              <p>User Name</p>
              <button className="btn btn-outline btn-error">Remove</button>
            </div>
            <div className="flex justify-between mt-2 items-center">
              <div style={{ width: 50, height: 50, backgroundColor: "whitesmoke", borderRadius: 10 }}>Av</div>
              <p>User Name</p>
              <button className="btn btn-outline btn-error">Remove</button>
            </div>
          </div>
        </div>
        {/* Sidebar content here */}
        <li><a>Sidebar Item 1</a></li>
        <li><a>Sidebar Item 2</a></li>
        <p>HI</p>
      </ul>
    </div>
  )
}