import { AddIcon, LockIcon, MoreOptionsIcon, SearchIcon, WorldIcon } from "../../assets/icons/Icons";
import { useBoardStore } from "../../context/useBoardStore";
import AvatarWithName from "../ui/AvatarWithName";
import { fakeUsers } from "../ui/Boards/boardsStore";

export default function SubHeader() {
  const { members } = useBoardStore(state => state)
  return (
    <header className="flex mb-4" >
      <div className="flex-1 flex gap-2">
        <details className="dropdown">
          <summary className="btn">
            <LockIcon />
            Private
          </summary>
          <ul className="p-2 shadow menu dropdown-content z-[1] bg-base-100 rounded-box w-64">
            <div className="p-1">
              <p className="text-lg font-semibold">Visibility</p>
              <span className="text-xs">Choose who can see this board</span>
            </div>
            <li><button className="btn mt-1 btn-lg">
              <div className="flex flex-col">
                <p className="flex items-center"> <WorldIcon /> Public </p>
                <p className="text-xs font-light mt-1">Anyone in the internet can see this</p>
              </div>
            </button></li>
            <li><button className="btn mt-1 btn-lg">
              <div className="flex flex-col">
                <p className="flex items-center"> <LockIcon /> Privatec </p>
                <p className="text-xs font-light mt-1">Only board menbers can see this</p>
              </div>
            </button></li>
            <li><a>Private</a></li>
          </ul>
        </details>
        {
          members.map(member => (
            <div className="avatar" key={`member-subh-${member.uid}`}>
              <div className="w-12 rounded">
                <img src={member.avatar} alt={member.name} className={`${true && "bg-green-300 place-items-center"}`} />
              </div>
            </div>
          ))
        }
        <details className="dropdown">
          <summary className="btn btn-accent">
            <AddIcon />
          </summary>
          <div className="p-3 shadow menu dropdown-content z-[1] bg-base-200 rounded w-72" >
            <div className="pb-2" >
              <p className="text-lg font-semibold">Invite to board</p>
              <p>Search users you want to invite to</p>
              <div className="flex mt-2">
                <input className="flex-1 input input-sm" placeholder="User..." />
                <button className="btn btn-sm btn-info" > <SearchIcon /> </button>
              </div>
            </div>
            {
              fakeUsers.map(n => <AvatarWithName
                key={`ava-name-${n.id}`}
                user={{ avatar: n.avatar, name: n.name, uid: n.id }}
                onClick={() => alert(`clicked ${n.name}`)}
              />)
            }
          </div>
        </details>
      </div>
      <div className="flex-1 flex justify-end">
        {/* <button className="btn">
          <MoreOptionsIcon />
          Show menu
        </button> */}
        <label htmlFor="my-drawer-4" className="drawer-button btn btn-primary">
          <MoreOptionsIcon />
          Open drawer
        </label>
      </div>
    </header>
  )
}

const FAKE_USERS = [
  {
    "id": 1,
    "name": "Leanne Graham",
    "username": "Bret",
    "email": "Sincere@april.biz",
    "address": {
      "street": "Kulas Light",
      "suite": "Apt. 556",
      "city": "Gwenborough",
      "zipcode": "92998-3874",
      "geo": {
        "lat": "-37.3159",
        "lng": "81.1496"
      }
    },
    "phone": "1-770-736-8031 x56442",
    "website": "hildegard.org",
    "company": {
      "name": "Romaguera-Crona",
      "catchPhrase": "Multi-layered client-server neural-net",
      "bs": "harness real-time e-markets"
    },
    avatar: "https://img.freepik.com/premium-photo/man-with-shirt-that-says-no-one-it_745528-3501.jpg?w=826"
  },
  {
    "id": 2,
    "name": "Ervin Howell",
    "username": "Antonette",
    "email": "Shanna@melissa.tv",
    "address": {
      "street": "Victor Plains",
      "suite": "Suite 879",
      "city": "Wisokyburgh",
      "zipcode": "90566-7771",
      "geo": {
        "lat": "-43.9509",
        "lng": "-34.4618"
      }
    },
    "phone": "010-692-6593 x09125",
    "website": "anastasia.net",
    "company": {
      "name": "Deckow-Crist",
      "catchPhrase": "Proactive didactic contingency",
      "bs": "synergize scalable supply-chains"
    },
    avatar: "https://img.freepik.com/premium-photo/woman-s-silhouette-is-shown-circle_771335-52785.jpg"
  },
]