import { ChangeEvent, useState } from "react"
import { AddIcon, PhotoIcon } from "../assets/icons/Icons"
import { Board } from "./ui/Boards/boardsStore"
import DropdownChooseVisibility from "./ui/dropdown/DropdownChooseVisibility";
import useBoards from "./ui/Boards/useBoards";
import CoverCard from "./ui/ModalCard/CoverCard";

export default function NewBoardModal() {
  const [board, setBoard] = useState<Board>(initialState)
  const { addBoard } = useBoards()

  function handleAddBoard() {
    addBoard(board)
  }
  function handleChange(e: ChangeEvent<HTMLInputElement>) {
    const { name, value } = e.target;
    setBoard({ ...board, [name]: value })
  }
  function handleClick() {
    const modal = document.getElementById('my_modal_3') as HTMLDialogElement
    if (modal) {
      modal.showModal()
    }
  }
  return (
    <div>
      {/* You can open the modal using document.getElementById('ID').showModal() method */}
      <button className="btn btn-primary" onClick={handleClick}>
        <AddIcon />
        Add
      </button>
      <dialog id="my_modal_3" className="modal">
        <div className="modal-box">
          <form method="dialog">
            {/* if there is a button in form, it will close the modal */}
            <button className="btn btn-primary btn-sm btn-circle absolute right-2 top-2">✕</button>
          </form>
          <img
            src={board.cover}
            className="w-full aspect-video rounded-lg"
            alt="cover" />
          <input placeholder="Add Board Title" className="input my-4 w-full" value={board.title} onChange={handleChange} name="title" />
          <div className="flex justify-between my-2 ">
            {/*
            <button className="flex flex-1 justify-center">
              <PhotoIcon />
              Cover
            </button>
            */}
            <CoverCard
              setCover={(cover: string) => { console.log(cover); setBoard({ ...board, cover }) }}
            />
            <div className="w-4" />
            {/*  <button className="flex flex-1 justify-center">
              <LockIcon />
              Private
  </button> */}
            <DropdownChooseVisibility
              value={board.visivility}
              onChange={(newVal) => setBoard({ ...board, visivility: newVal })}
              className="flex-1"
            />
          </div>
          <div className="flex justify-end mt-6">
            <button
              className="btn btn-ghost"
              onClick={handleClick}
            >
              Cancel
            </button>
            <button className="btn btn-primary ml-2" onClick={handleAddBoard} >
              <AddIcon />
              Create
            </button>
          </div>
          <h3 className="font-bold text-lg">Hello!</h3>
          <p className="py-4">Press ESC key or click on ✕ button to close</p>
        </div>
      </dialog>
    </div>
  )
} 

const default_cover = "https://images.unsplash.com/photo-1515378791036-0648a3ef77b2?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTV8fG9mZmljZXxlbnwwfHwwfHx8MA%3D%3D"

const initialState: Board = {
  cover: default_cover,
  created: {
    createdBy: {
      author: {
        name: "",
        avatar: "",
        id: ""
      }
    },
    createdAt: new Date(),
  },
  members: [],
  description: "",
  title: "",
  visivility: "private",
}