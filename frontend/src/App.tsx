import './App.css'
import AllBoardsPage from './components/pages/AllBoardsPage'
import HomePage from './components/pages/HomePage'
import { MainPage } from './components/pages/MainPage'
import { RouterProvider, createBrowserRouter, useRouteError } from 'react-router-dom'


const router = createBrowserRouter([
  {
    path: "/",
    element: <HomePage />
  }, {
    path: "/main",
    element: <MainPage />,
    errorElement: <ErrorBoundary />
  }, {
    path: "/boards",
    element: <AllBoardsPage />
  }
])

function App() {
  return (
    <RouterProvider router={router} />
  )
}

export default App

function ErrorBoundary() {
  let error = useRouteError();
  console.error(error);
  // Uncaught ReferenceError: path is not defined
  return <div>Dang!</div>;
}