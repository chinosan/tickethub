import { create } from "zustand";
import { BoardItem, DNDType, ListBoard, Member } from "../types/BoardTypes";
import { UniqueIdentifier } from "@dnd-kit/core";
import { MEMBERS_SAMPLE } from "../constants/constants";

type BoardProps = {
  containers: DNDType[],
  addContainer: (container: DNDType) => void,
  // addTask:()=>void
  resetContainers: (newContainers: DNDType[]) => void
  changeTaskContainer?: (idTask: UniqueIdentifier, idOldContainer: UniqueIdentifier, idNewContainer: UniqueIdentifier) => void,
  currentTask: BoardItem | null,
  setCurrentTask: (task: BoardItem) => void,
  clearCurrentTask: () => void,
  members: Member[],
  setMembers: (newMember: Member) => void
}

export const useBoardStore = create<BoardProps>()((set) => ({
  containers: [{id:`container-${crypto.randomUUID()}`,items:[],title:"Backlog"}],
  addContainer: (newContainer) => set((state) => ({ containers: [...state.containers, newContainer] })),
  resetContainers: (newContainers) => set({ containers: [...newContainers] }),
  currentTask: null,
  setCurrentTask: (task) => set({ currentTask: task }),
  clearCurrentTask: () => set({ currentTask: null }),
  members: [...MEMBERS_SAMPLE],
  setMembers: (newMember) => set((state) => ({ members: [...state.members, newMember] }))
}))
