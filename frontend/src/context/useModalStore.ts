import { create } from "zustand";

type ModalStoreProps = {
  isModalCardVisible: boolean,
  toggleIsModalCardVisible: () => void,
  openModal: () => void
}

export const useModalStore = create<ModalStoreProps>()((set) => ({
  isModalCardVisible: false,
  toggleIsModalCardVisible: () => set((state) => ({ isModalCardVisible: !state.isModalCardVisible })),
  openModal: () => set({ isModalCardVisible: true })
}))