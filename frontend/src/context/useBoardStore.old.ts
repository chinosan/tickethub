import { create } from "zustand";
import { ListBoard } from "../types/BoardTypes";
import { UniqueIdentifier } from "@dnd-kit/core";

type Boarprops = {
  containers: ListBoard[],
  addContainer: (container: ListBoard) => void,
  changeTaskContainer: (idTask:UniqueIdentifier, idOldContainer:UniqueIdentifier, idNewContainer:UniqueIdentifier) => void
}

const useBoardStore = create<Boarprops>((set) => ({
  containers: [],
  addContainer: (newContainer) => set((state) => ({ containers: [...state.containers, newContainer] })),
  changeTaskContainer: (idTask, idOld, idnew)=>set( (state) => {
    const oldContainer = state.containers.find( c=> c.id==idOld)
    if(oldContainer){
      oldContainer.items.filter( item => item.id != idTask)
    }
    const newContainer = state.containers.find( c=> c.id==idnew)
    if(newContainer){
      // newContainer.items.push()
    }
    return {}
  })
}))