import { create } from "zustand";

type BearsProps = {
  bears: number,
  increasePopulation: () => void,
  removeAllBears: () => void,
  increaseByAmount: (amount: number) => void
}

export const useBearsStore = create<BearsProps>((set) => ({
  bears: 0,
  increasePopulation: () => set((state) => ({ bears: state.bears + 1 })),
  removeAllBears: () => set({ bears: 0 }),
  increaseByAmount: (amount) => set((state) => ({ bears: state.bears + amount }))
}))

type Task = {
  id: string,
  title: string,

}
type TaskStoreProps = {
  data: Task[],
  addTask: (newTask: Task) => void,
  removeTask: (idTask: string) => void
}
export const useTaskStore = create<TaskStoreProps>((set) => ({
  data: [],
  addTask: (newTask) => set((state) => ({ data: [...state.data, newTask] })),
  removeTask: (idTask) => set((state) => ({ data: state.data.filter(task => task.id != idTask) }))
}))